# Notes

- https://github.com/microsoft/vscode-textmate
- https://github.com/shama/napa
- https://github.com/unjs/scule
- https://www.npmjs.com/package/just-kebab-case
- https://www.rainerhahnekamp.com/en/type-safe-typescript-with-type-narrowing/#92-automatic-validation-zod
- https://transform.tools/typescript-to-zod
- https://github.com/colinhacks/zod/issues/53
- https://blog.dennisokeeffe.com/blog/2023-06-16-ensuring-type-safety-with-type-guards
- https://dev.to/sachitsac/typescript-type-guards-with-zod-1m12
- https://oxc-project.github.io/docs/guide/usage/linter.html
- https://biomejs.dev/linter/rules/
- https://biomejs.dev/reference/cli/#biome-check
- https://globster.xyz/
- https://biomejs.dev/linter/#ignoring-code
- https://www.typescriptlang.org/docs/handbook/2/classes.html#this-based-type-guards
- https://www.typescriptlang.org/docs/handbook/2/narrowing.html#typeof-type-guards
- https://www.typescriptlang.org/docs/handbook/2/narrowing.html#using-type-predicates
- https://github.com/sniptt-official/guards
- https://www.npmjs.com/package/ts-type-guards
- https://github.com/SimonAlling/ts-type-guards/blob/v0.7.0/src/is.ts
- https://github.com/jellydn/typescript-tips
- https://www.matthewgerstman.com/tech/ts-tricks-type-guards/
- https://github.com/davidkarolyi/tguard
- https://github.com/biomejs/biome/issues/237#issuecomment-1772682137 + https://github.com/biomejs/biome/issues/237#issuecomment-1772751058 + https://github.com/biomejs/biome/blob/cli/v1.4.1/crates/biome_service/src/file_handlers/mod.rs#L48 + https://github.com/biomejs/biome/issues/237#issuecomment-1774683915
- https://github.com/biomejs/biome/blob/cli/v1.4.1/website/src/content/docs/guides/how-biome-works.mdx#L67
- https://biomejs.dev/guides/how-biome-works/#known-files (`package.json` is not formatted)
- https://amberley.dev/blog/2020-09-07-conditionally-add-to-array-or-obj/
- https://www.codemzy.com/blog/conditionally-add-property-to-object-javascript
- https://developer.mozilla.org/en-US/docs/Glossary/Falsy
- Shiki:
  - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/types.ts#L157
  - `getThemeDefaultColors()`: https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L223
  - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/highlighter.ts#L95
  - `toShikiTheme()`: https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L194
  - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L147
  - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/index.ts#L12
  - `IShikiTheme`: https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/types.ts#L159
- `"format:pkg": "cp package.json formatter.json && biome format --write formatter.json && cp formatter.json package.json"`
- https://zod.dev/?id=type-inference
- https://zod.dev/?id=safeparse
- https://zod.dev/?id=parse
- https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
- https://github.com/sindresorhus/index-to-position/blob/main/package.json
- https://github.com/sindresorhus/get-east-asian-width
- https://github.com/sindresorhus/get-east-asian-width/blob/main/scripts/build.js
- https://github.com/sindresorhus/get-east-asian-width/blob/main/index.d.ts#L60
- https://github.com/sindresorhus/type-fest/blob/main/index.d.ts
- https://github.com/sindresorhus/type-fest/blob/main/source/primitive.d.ts
- https://github.com/sindresorhus/type-fest/blob/main/source/keys-of-union.d.ts
- https://github.com/sxzz/ts-lib-starter
- https://github.com/egoist/ts-lib-starter
- https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c
- https://github.com/sindresorhus/typescript-definition-style-guide
- https://docs.npmjs.com/cli/v10/configuring-npm/package-json#engines
- https://tsdoc.org/
- https://github.com/uhyo/eslint-plugin-import-access
- https://www.npmjs.com/package/outdent
- https://www.typescriptlang.org/docs/handbook/declaration-files/templates/module-d-ts.html#common-commonjs-patterns
- https://github.com/sindresorhus/stdin-discarder/blob/main/index.d.ts + https://github.com/sindresorhus/stdin-discarder/blob/main/index.js
- https://www.typescriptlang.org/docs/handbook/declaration-files/by-example.html#global-variables
- https://github.com/TypeScriptToLua/TypeScriptToLua
- https://github.com/sindresorhus/stdin-discarder/blob/main/index.d.ts
- https://github.com/sindresorhus/debounce/blob/main/index.d.ts
- https://github.com/sindresorhus/p-debounce/blob/main/index.js + https://github.com/sindresorhus/p-debounce/blob/main/index.d.ts: `const pDebounce` + `export default pDebounce;` + `declare const pDebounce`
- https://github.com/sindresorhus/find-up-simple/blob/main/index.d.ts + https://github.com/sindresorhus/find-up-simple/blob/main/index.js: `export async function findUp()` + `export function findUpSync()` + `export function findUp(name: string, options?: Options): Promise<string | undefined>;` + `export function findUpSync(name: string, options?: Options): string | undefined`
- https://github.com/sindresorhus/ora/blob/main/index.d.ts
- https://github.com/sindresorhus/cli-spinners/blob/main/index.d.ts + https://github.com/sindresorhus/cli-spinners/blob/main/index.js
- https://github.com/sindresorhus/alfy/blob/main/index.d.ts
- https://github.com/sindresorhus/uint8array-extras/blob/main/index.d.ts: `export function ...`
- `"sideEffects": false`:
  - https://www.rspack.dev/guide/tree-shaking#sideeffects
  - https://webpack.js.org/guides/tree-shaking/#mark-the-file-as-side-effect-free
- Compiler API:
  - https://github.com/microsoft/TypeScript/blob/v5.3.3/src/compiler/factory/nodeFactory.ts#L4449
  - https://writer.sh/posts/gentle-introduction-to-typescript-compiler-api/
  - https://nabeelvalley.co.za/docs/javascript/typescript-ast/
  - https://github.com/dsherret/ts-ast-viewer + https://ts-ast-viewer.com/ + https://github.com/dsherret/ts-factory-code-generator-generator/
  - https://github.com/microsoft/TypeScript/wiki/Using-the-Compiler-API
  - https://blog.cloudflare.com/improving-workers-types
  - https://blog.pcarleton.com/post/buckle-typings/
  - https://www.npmjs.com/package/ts-morph + https://github.com/dsherret/ts-morph
  - https://github.com/dsherret/ts-morph/tree/latest/packages/bootstrap
  - https://learning-notes.mistermicheels.com/javascript/typescript/compiler-api/
- https://github.com/horiuchi/dtsgenerator
- https://github.com/npm/ini
- https://github.com/dsherret/dax (alternative to [zx](https://github.com/google/zx))
- https://stackoverflow.com/questions/55246585/how-to-generate-extra-newlines-between-nodes-with-the-typescript-compiler-api-pr:
  - `ts.factory.createIdentifier("\n");`
- https://github.com/antfu/textmate-grammars-themes

## Commands

```bash
npm install -D jiti
```

```bash
npm install -D fs-extra @types/fs-extra
```

```bash
npm install -D vscode-textmate
```

```bash
npm install -D plist @types/plist
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

```bash
rm -rf node_modules/ && npm install
```

```bash
npm install -D scule
```

```bash
npm install -D just-kebab-case just-camel-case
```

```bash
npm install -D github:microsoft/vscode-textmate#8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5
```

```bash
npm install -D zod
```

```bash
npm install -D @biomejs/biome
```

```bash
npm install -D shiki
```

```bash
cp package.json formatter.json && biome format --write formatter.json && cp formatter.json package.json
```

```bash
npm install -D npm-run-all2
```

```bash
npm pack
```

```bash
npm install -D typescript
```

## Snippets

### https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/theme.ts#L78

```ts
export type ScopePattern = string;

export interface IRawTheme {
  readonly name?: string;
  readonly settings: IRawThemeSetting[];
}

export interface IRawThemeSetting {
  readonly name?: string;
  readonly scope?: ScopePattern | ScopePattern[];
  readonly settings: {
    readonly fontStyle?: string;
    readonly foreground?: string;
    readonly background?: string;
  };
}
```

### `processScope()` helper function

```ts
function isString(x: unknown): x is string {
  return typeof x === "string";
}

function processScope(scope: string | string[]): string | string[] {
  const processedScope: string[] = isString(scope)
    ? scope.split(",").map((scopeValue) => scopeValue.trim())
    : scope;

  const finalScope: string | string[] =
    processedScope.length === 1 ? processedScope[0] : processedScope;

  return finalScope;
}
```

### Comments

```ts
// https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/main.ts
// https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/parseRawGrammar.ts
// https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/parseRawGrammar.ts#L10
// https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/parseRawGrammar.ts#L28: `return <IRawGrammar>plist.parsePLIST(contents);`
// https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/plist.ts#L33
const parsedTheme = parse(themeContent);
// console.log(JSON.stringify(parsedTheme, undefined, 2));

// https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/themes/monokai.json
// https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/themes/nord.json
// https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/themes/css-variables.json
// https://macromates.com/manual/en/scope_selectors
// https://macromates.com/manual/en/scope_selectors#grouping
// https://macromates.com/manual/en/language_grammars#naming_conventions
if (isValidTextMateTheme(parsedTheme)) {
  // ...
}
```

### https://github.com/sindresorhus/ora/blob/main/package.json vs. https://github.com/sindresorhus/stdin-discarder/blob/main/package.json

```json
{
  "type": "module",
  "exports": "./index.js",
  "engines": {
    "node": ">=16"
  },
  "files": ["index.js", "index.d.ts"]
}
```

```json
{
  "type": "module",
  "exports": {
    "types": "./index.d.ts",
    "default": "./index.js"
  },
  "sideEffects": false,
  "engines": {
    "node": ">=18"
  },
  "files": ["index.js", "index.d.ts"]
}
```

### Deprecated

```ts
const shikiThemeVariable = [
  "// @ts-check",
  "",
  "/**",
  ' * @type {import("../index").ShikiExtraTheme}',
  " */",
  `export const tomorrowNight = ${JSON.stringify(shikiTheme)};`,
].join("\n");
```

```js
// @ts-check

/**
 * @type {import("../index").ShikiExtraTheme}
 */
export const tomorrowNight = {};
```
