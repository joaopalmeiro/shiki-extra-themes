import { readFileSync, writeFileSync } from "node:fs";
import { basename } from "node:path";
import camelCase from "just-camel-case";
import { parse } from "plist";
import type { IShikiTheme } from "shiki";
import type { VariableStatement } from "typescript";
import {
  ListFormat,
  NewLineKind,
  NodeFlags,
  ScriptKind,
  ScriptTarget,
  SyntaxKind,
  createPrinter,
  createSourceFile,
  factory,
} from "typescript";

import type { ShikiExtraTheme } from "../index";
import {
  EXTRA_THEMES_DECLARATION,
  MAIN_DECLARATION_FILE,
  MAIN_INDEX,
  SHIKI_EXTRA_THEME_INTERFACE,
  THEMES,
} from "./constants";
import type { TextMateThemeSchema } from "./utils";
import { getThemeDefaultColors, isUndefined, isValidTextMateTheme } from "./utils";

function main(): void {
  const shikiThemes: string[] = [];
  const shikiThemeTypes: VariableStatement[] = [];
  const shikiThemeNames: string[] = [];

  for (const theme of THEMES) {
    const themeContent = readFileSync(theme.source, { encoding: "utf8" });
    const parsedTheme = parse(themeContent);

    if (isValidTextMateTheme(parsedTheme)) {
      const themeSettings = parsedTheme.settings.map((setting) => {
        const {
          name,
          scope,
          settings: { fontStyle, foreground, background },
        } = setting;

        const themeSettingSettings: TextMateThemeSchema["settings"][number]["settings"] = {
          ...(!isUndefined(fontStyle) && fontStyle !== "" && { fontStyle }),
          ...(!isUndefined(foreground) && { foreground: foreground.toLowerCase() }),
          ...(!isUndefined(background) && { background: background.toLowerCase() }),
        };

        const themeSetting: TextMateThemeSchema["settings"][number] = {
          ...(!isUndefined(name) && { name }),
          ...(!isUndefined(scope) && { scope }),
          settings: themeSettingSettings,
        };

        return themeSetting;
      });

      const shikiTheme: ShikiExtraTheme = {
        name: theme.name,
        type: theme.type,
        settings: themeSettings,
        ...getThemeDefaultColors(themeSettings, theme.type),
      } satisfies IShikiTheme;

      const varName = camelCase(theme.name);

      const shikiThemeVar = `export const ${varName} = ${JSON.stringify(shikiTheme)};`;
      shikiThemes.push(shikiThemeVar);

      const shikiThemeType: VariableStatement = factory.createVariableStatement(
        [factory.createToken(SyntaxKind.ExportKeyword)],
        factory.createVariableDeclarationList(
          [
            factory.createVariableDeclaration(
              factory.createIdentifier(varName),
              undefined,
              factory.createTypeReferenceNode(
                factory.createIdentifier("ShikiExtraTheme"),
                undefined,
              ),
              undefined,
            ),
          ],
          NodeFlags.Const,
        ),
      );
      shikiThemeTypes.push(shikiThemeType);

      // Source: https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/themes.ts
      shikiThemeNames.push(theme.name);
    }
  }

  const indexContent = [
    ...shikiThemes,
    `export const extraThemes = ${JSON.stringify(shikiThemeNames)}`,
  ].join("\n");
  writeFileSync(MAIN_INDEX, indexContent);

  // biome-ignore lint/suspicious/noConsoleLog: to confirm the creation of the index.js file and its path
  console.log(`${MAIN_INDEX} ✓`);

  const extraThemesType = factory.createTypeAliasDeclaration(
    [factory.createToken(SyntaxKind.ExportKeyword)],
    factory.createIdentifier("ExtraTheme"),
    undefined,
    factory.createUnionTypeNode([
      ...shikiThemeNames.map((n) => factory.createLiteralTypeNode(factory.createStringLiteral(n))),
    ]),
  );

  const typeNodes = factory.createNodeArray([
    SHIKI_EXTRA_THEME_INTERFACE,
    ...shikiThemeTypes,
    extraThemesType,
    EXTRA_THEMES_DECLARATION,
  ]);

  // const declarationFile = createSourceFile(basename(MAIN_DECLARATION_FILE), "", ScriptTarget.ESNext);
  // or
  const declarationFile = createSourceFile(
    basename(MAIN_DECLARATION_FILE),
    "",
    ScriptTarget.ESNext,
    false,
    ScriptKind.TS,
  );

  const printer = createPrinter({ newLine: NewLineKind.LineFeed });
  const outputDeclarationFile = printer.printList(ListFormat.MultiLine, typeNodes, declarationFile);

  writeFileSync(MAIN_DECLARATION_FILE, outputDeclarationFile);

  // biome-ignore lint/suspicious/noConsoleLog: to confirm the creation of the index.d.ts file and its path
  console.log(`${MAIN_DECLARATION_FILE} ✓`);
}

main();
