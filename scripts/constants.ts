import { resolve } from "node:path";
import { NodeFlags, SyntaxKind, factory } from "typescript";

import type { ShikiExtraTheme } from "../index";

export const MAIN_INDEX = resolve(__dirname, "../index.js");
export const MAIN_DECLARATION_FILE = resolve(__dirname, "../index.d.ts");

interface Theme {
  readonly source: string;
  readonly name: string;
  readonly type: ShikiExtraTheme["type"];
}

export const THEMES: Theme[] = [
  {
    source: resolve(__dirname, "../tomorrow-theme/textmate/Tomorrow-Night.tmTheme"),
    name: "tomorrow-night",
    type: "dark",
  },
];

// Source: https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L217
export const VSCODE_FALLBACK_EDITOR_FG = { light: "#333333", dark: "#bbbbbb" };
export const VSCODE_FALLBACK_EDITOR_BG = { light: "#fffffe", dark: "#1e1e1e" };

// Helper tool: https://ts-ast-viewer.com/
export const SHIKI_EXTRA_THEME_INTERFACE = factory.createInterfaceDeclaration(
  [factory.createToken(SyntaxKind.ExportKeyword)],
  factory.createIdentifier("ShikiExtraTheme"),
  undefined,
  undefined,
  [
    factory.createPropertySignature(
      undefined,
      factory.createIdentifier("name"),
      undefined,
      factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
    ),
    factory.createPropertySignature(
      undefined,
      factory.createIdentifier("type"),
      undefined,
      factory.createUnionTypeNode([
        factory.createLiteralTypeNode(factory.createStringLiteral("light")),
        factory.createLiteralTypeNode(factory.createStringLiteral("dark")),
      ]),
    ),
    factory.createPropertySignature(
      undefined,
      factory.createIdentifier("settings"),
      undefined,
      factory.createArrayTypeNode(
        factory.createTypeLiteralNode([
          factory.createPropertySignature(
            undefined,
            factory.createIdentifier("name"),
            factory.createToken(SyntaxKind.QuestionToken),
            factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
          ),
          factory.createPropertySignature(
            undefined,
            factory.createIdentifier("scope"),
            factory.createToken(SyntaxKind.QuestionToken),
            factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
          ),
          factory.createPropertySignature(
            undefined,
            factory.createIdentifier("settings"),
            undefined,
            factory.createTypeLiteralNode([
              factory.createPropertySignature(
                undefined,
                factory.createIdentifier("fontStyle"),
                factory.createToken(SyntaxKind.QuestionToken),
                factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
              ),
              factory.createPropertySignature(
                undefined,
                factory.createIdentifier("foreground"),
                factory.createToken(SyntaxKind.QuestionToken),
                factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
              ),
              factory.createPropertySignature(
                undefined,
                factory.createIdentifier("background"),
                factory.createToken(SyntaxKind.QuestionToken),
                factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
              ),
            ]),
          ),
        ]),
      ),
    ),
    factory.createPropertySignature(
      undefined,
      factory.createIdentifier("fg"),
      undefined,
      factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
    ),
    factory.createPropertySignature(
      undefined,
      factory.createIdentifier("bg"),
      undefined,
      factory.createKeywordTypeNode(SyntaxKind.StringKeyword),
    ),
  ],
);

export const EXTRA_THEMES_DECLARATION = factory.createVariableStatement(
  [factory.createToken(SyntaxKind.ExportKeyword)],
  factory.createVariableDeclarationList(
    [
      factory.createVariableDeclaration(
        factory.createIdentifier("extraThemes"),
        undefined,
        factory.createArrayTypeNode(
          factory.createTypeReferenceNode(factory.createIdentifier("ExtraTheme"), undefined),
        ),
        undefined,
      ),
    ],
    NodeFlags.Const,
  ),
);
