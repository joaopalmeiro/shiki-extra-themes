import { z } from "zod";

import type { ShikiExtraTheme } from "../index";
import { VSCODE_FALLBACK_EDITOR_BG, VSCODE_FALLBACK_EDITOR_FG } from "./constants";

// Based on: https://github.com/microsoft/vscode-textmate/blob/8b07a3c2be6fe4674f9ce6bba6d5c962a7f50df5/src/theme.ts#L78
const textMateSettingsSchema = z.object({
  name: z.string().optional(),
  // scope: z.union([z.string(), z.array(z.string())]).optional(),
  scope: z.string().optional(),
  settings: z.object({
    fontStyle: z.string().optional(),
    foreground: z.string().optional(),
    background: z.string().optional(),
  }),
});

export const textMateThemeSchema = z.object({
  name: z.string().optional(),
  settings: z.array(textMateSettingsSchema),
});

export type TextMateThemeSchema = z.infer<typeof textMateThemeSchema>;

export function isValidTextMateTheme(value: unknown): value is TextMateThemeSchema {
  return textMateThemeSchema.safeParse(value).success;
}

// TODO: Get this function from the tguards package
export function isUndefined(x: unknown): x is undefined {
  return x === undefined;
}

// Source:
// - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L201
// - https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/src/loader.ts#L223
export function getThemeDefaultColors(
  settings: TextMateThemeSchema["settings"],
  type: ShikiExtraTheme["type"],
): { fg: string; bg: string } {
  let fg = type === "light" ? VSCODE_FALLBACK_EDITOR_FG.light : VSCODE_FALLBACK_EDITOR_FG.dark;
  let bg = type === "light" ? VSCODE_FALLBACK_EDITOR_BG.light : VSCODE_FALLBACK_EDITOR_BG.dark;

  const globalSetting = settings.find((s) => {
    // return !s.name && !s.scope;
    // vs.
    return !(s.name || s.scope);
  });

  if (globalSetting?.settings.foreground) {
    fg = globalSetting.settings.foreground;
  }
  if (globalSetting?.settings.background) {
    bg = globalSetting.settings.background;
  }

  return { fg, bg };
}
