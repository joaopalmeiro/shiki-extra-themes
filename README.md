# shiki-extra-themes

Extra themes for [Shiki](https://github.com/shikijs/shiki). Tested on Shiki [v0.14.6](https://github.com/shikijs/shiki/releases/tag/v0.14.6).

- [Source code](https://codeberg.org/joaopalmeiro/shiki-extra-themes)
- [npm package](https://www.npmjs.com/package/shiki-extra-themes)
- [Documentation](https://tsdocs.dev/docs/shiki-extra-themes/0.0.6)

## Available themes

| Theme                                          | Source                                                                                                               | Version                                  |
| ---------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| [`tomorrow-night`](themes/tomorrow-night.json) | [Tomorrow Night](https://github.com/chriskempson/tomorrow-theme) by [Chris Kempson](https://github.com/chriskempson) | ccf6666d888198d341b26b3a99d0bc96500ad503 |

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npx degit chriskempson/tomorrow-theme#ccf6666d888198d341b26b3a99d0bc96500ad503 tomorrow-theme
```

```bash
npm run build
```

```bash
npm run format
```

```bash
npm pack --dry-run
```

## Deployment

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the package version in the _Documentation_ URL at the top of the README file.
- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [Codeberg](https://codeberg.org/joaopalmeiro/shiki-extra-themes/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/shiki-extra-themes).
