export interface ShikiExtraTheme {
  name: string;
  type: "light" | "dark";
  settings: {
    name?: string;
    scope?: string;
    settings: {
      fontStyle?: string;
      foreground?: string;
      background?: string;
    };
  }[];
  fg: string;
  bg: string;
}
export const tomorrowNight: ShikiExtraTheme;
export type ExtraTheme = "tomorrow-night";
export const extraThemes: ExtraTheme[];
