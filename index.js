export const tomorrowNight = {
  name: "tomorrow-night",
  type: "dark",
  settings: [
    { settings: { foreground: "#c5c8c6", background: "#1d1f21" } },
    { name: "Comment", scope: "comment", settings: { foreground: "#969896" } },
    {
      name: "Foreground",
      scope: "keyword.operator.class, constant.other, source.php.embedded.line",
      settings: { foreground: "#ced1cf" },
    },
    {
      name: "Variable, String Link, Regular Expression, Tag Name, GitGutter deleted",
      scope:
        "variable, support.other.variable, string.other.link, string.regexp, entity.name.tag, entity.other.attribute-name, meta.tag, declaration.tag, markup.deleted.git_gutter",
      settings: { foreground: "#cc6666" },
    },
    {
      name: "Number, Constant, Function Argument, Tag Attribute, Embedded",
      scope:
        "constant.numeric, constant.language, support.constant, constant.character, variable.parameter, punctuation.section.embedded, keyword.other.unit",
      settings: { foreground: "#de935f" },
    },
    {
      name: "Class, Support",
      scope: "entity.name.class, entity.name.type.class, support.type, support.class",
      settings: { foreground: "#f0c674" },
    },
    {
      name: "String, Symbols, Inherited Class, Markup Heading, GitGutter inserted",
      scope:
        "string, constant.other.symbol, entity.other.inherited-class, entity.name.filename, markup.heading, markup.inserted.git_gutter",
      settings: { foreground: "#b5bd68" },
    },
    {
      name: "Operator, Misc",
      scope: "keyword.operator, constant.other.color",
      settings: { foreground: "#8abeb7" },
    },
    {
      name: "Function, Special Method, Block Level, GitGutter changed",
      scope:
        "entity.name.function, meta.function-call, support.function, keyword.other.special-method, meta.block-level, markup.changed.git_gutter",
      settings: { foreground: "#81a2be" },
    },
    {
      name: "Keyword, Storage",
      scope: "keyword, storage, storage.type, entity.name.tag.css",
      settings: { foreground: "#b294bb" },
    },
    {
      name: "Invalid",
      scope: "invalid",
      settings: { foreground: "#ced2cf", background: "#df5f5f" },
    },
    {
      name: "Separator",
      scope: "meta.separator",
      settings: { foreground: "#ced2cf", background: "#82a3bf" },
    },
    {
      name: "Deprecated",
      scope: "invalid.deprecated",
      settings: { foreground: "#ced2cf", background: "#b798bf" },
    },
    {
      name: "Diff foreground",
      scope:
        "markup.inserted.diff, markup.deleted.diff, meta.diff.header.to-file, meta.diff.header.from-file",
      settings: { foreground: "#ffffff" },
    },
    {
      name: "Diff insertion",
      scope: "markup.inserted.diff, meta.diff.header.to-file",
      settings: { foreground: "#718c00" },
    },
    {
      name: "Diff deletion",
      scope: "markup.deleted.diff, meta.diff.header.from-file",
      settings: { foreground: "#c82829" },
    },
    {
      name: "Diff header",
      scope: "meta.diff.header.from-file, meta.diff.header.to-file",
      settings: { foreground: "#ffffff", background: "#4271ae" },
    },
    {
      name: "Diff range",
      scope: "meta.diff.range",
      settings: { fontStyle: "italic", foreground: "#3e999f" },
    },
  ],
  fg: "#c5c8c6",
  bg: "#1d1f21",
};
export const extraThemes = ["tomorrow-night"];
